<%@ control Language="VB"  AutoEventWireup="false" CodeFile="courseapply.ascx.vb" Inherits="courseapply"  %>
<%@ Register Assembly="PSWebEnrolmentKit" Namespace="CompassCC.ProSolution.PSWebEnrolmentKit"
    TagPrefix="cc1" %>

<br /><br />
                <ol class="breadcrumb">
                  <li><a href="http://www.liv-coll.ac.uk/All_Courses.aspx">Search</a></li>
                  <li class="active">Course Details</li>
                </ol>

    <!--The CourseApplyAction adds the offering to the basket as an application-->
    <cc1:CourseApplyAction ID="CourseApplyAction" runat="server" />
    <h3>The following Course has been added to your application:</h3> 
	<!--<h3>Course Description: <%=courseDesc %></h3>-->
    </br>
    

 <cc1:OfferingFeesDisplayAll runat="server"  />

<p>
        At the City of Liverpool College there are no upfront fees to pay.
    </p>
    <p>
        Our Advice & Guidance team are available to provide information on a range of flexible payment options and we have a dedicated team to discuss queries relating to fees, funding and finance. We will contact you to book an appointment on receipt of your application.</p>
    <p>
        <b>Total courses selected:</b> &nbsp;<span class="wglyphicon glyphicon-shopping-cart"></span><cc1:ShoppingBasketTotals ID="ShoppingBasket1"
            runat="server" HideIcon="true" />
    </p>
<div>
    &nbsp;
</div>

<table>
      <tr>
         <td style="text-align:left; vertical-align:top;   width:300px; padding-top:0px">
             <i>If you want to pick additional courses then return to the 'Course Finder' page by clicking the 'Search' button.</i>
        </td>
        <td style="text-align:left; vertical-align:top;  width:300px;padding-left:50px;">
            <i>To complete your application we need to collect some of your personal details; click the 'Next' button to proceed.</i>
        </td> 
    </tr>
    <tr>
        <td style="text-align:left;">
            <cc1:CCCButton id="CCCButton3" runat="server" Text="Search for more courses" ImageResource="btnSearch" LinkResource="COLC_Courses"/>
         </td>
          <td style="padding-left:50px; text-align:left;"">
            <cc1:CCCButton ID="CCCButton2" runat="server" Text="Continue to complete application" ImageResource="btnContinue" LinkResource="checkout_aspx" CausesValidation="true" />
        </td>
    </tr>
  
</table>
<p></p>
<!--
    If you want to pick additional courses then return to the 'Course Finder' page by clicking the 'Search' button:
<div>        
    <cc1:CCCButton id="btnSearch" runat="server" Text="Search for more courses" ImageResource="btnSearch" LinkResource="COLC_Courses"/>
 </div> 
<div>
    To complete your application we need to collect some of your personal details; click the 'Next' button to proceed: 
</div>
<div>   
       <cc1:CCCButton ID="btnContinue" runat="server" Text="Continue to Checkout" ImageResource="btnContinue" LinkResource="checkout_aspx" CausesValidation="true" />
</div>
-->
<script>
    $(document).ready(function () {
        $('#FeeGrid tr').find('td:eq(2),th:eq(2)').hide();
        $("#FeeGrid td:contains('Standard')").html("16-18");
        $("#FeeGrid td:contains('24+')").html("Adult");
       	$("#FeeGrid").find('tr:gt(1)').find('td:eq(1)').html("FREE");
		$("#FeeGridTotalCell").attr('align','center');
    });
</script>

