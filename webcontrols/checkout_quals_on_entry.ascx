﻿<%@ control  Language="VB"  AutoEventWireup="false" CodeFile="checkout_quals_on_entry.ascx.vb" Inherits="checkout_quals_on_entry" %>
<%@ Register Assembly="PSWebEnrolmentKit" Namespace="CompassCC.ProSolution.PSWebEnrolmentKit"
    TagPrefix="cc1" %>

<br /><br />
                <ol class="breadcrumb" id="breadcrumbapps" runat="server">
                 <li><a href="http://www.liv-coll.ac.uk/All_Courses.aspx">Search</a></li>
                    <li><a href="#" onclick="window.history.go(-6);return false;">Course Details</a></li>
                    <li><a href="#" onclick="window.history.go(-5);return false;">Your Courses</a></li>
					<li><a href="#" onclick="window.history.go(-4);return false;">Personal Details</a></li>
					<li><a href="#" onclick="window.history.go(-3);return false;">Further Details</a></li>
					<li><a href="#" onclick="window.history.go(-2);return false;">School/Employer</a></li>
					<li><a href="#" onclick="window.history.go(-1);return false;">Prior Attainment</a></li>
                  <li class="active">Predicted Grades</li>
                </ol>
				
                 <ol class="breadcrumb" id="breadcrumbenrols" runat="server">
                  <li><a href="default.aspx">Home</a></li>
                    <li><a href="webenrolment.aspx?page=~/webcontrols/checkout.ascx">Checkout</a></li>
                    <li><a href="webenrolment.aspx?page=~/webcontrols/checkout_enrolments.ascx">Personal Details</a></li>
                   
                    <li><a href="webenrolment.aspx?page=~/webcontrols/checkout_enrolments2.ascx">Further Details</a></li>
                    <li><a href="webenrolment.aspx?page=~/webcontrols/checkout_parent_guardian.ascx">Parent / Carer</a></li>
                    <li><a href="webenrolment.aspx?page=~/webcontrols/checkout_school_employer.ascx">School / Employer</a></li>
                    <li><a href="webenrolment.aspx?page=~/webcontrols/checkout_prior_attainment.ascx">Prior Attainment</a></li>
                  <li class="active">Predicted Grades</li>
                </ol>

<div class="panel panel-info" id="panel1" runat="server">
    <div class="panel-heading">Predicted Grades</div>
     
   <p>If you have not yet taken your exams, please provide us with your predicted grades. 
                Please include information on all qualifications. ie GCSE, NVQs, BTEC, Diplomas, A levels or any other subjects you may have studied. </p> 
                <p>
                Please enter the details of your prior qualifications, choosing from the 
                drop-down lists below.</p>

    <a href="#" class="show_hide btn btn-primary">No prior qualifications</a>
    <br />
<br />
    <div id="slidingdiv" class="slidingDiv" runat="server">
<asp:table class="table table-striped text-center" id="tblQuals" runat="server">
<%--<tr><th>Qualification</th><th>Subject (if not in list)</th><th>Predicted Grade</th><th>Grade</th><th>Date Awarded</th></tr>--%>

</asp:table>
    <asp:button runat="server" id="btnAdd" cssClass="btn btn-success" text="Add Row"></asp:button>

    </div>
   
    </div>

 <cc1:CCCButton ID="btnBack" runat="server" Text="Back" ImageResource="btnBack" LinkResource="checkout_priorattainment_aspx" />
<cc1:CCCButton ID="btnContinue" runat="server" Text="Continue" ImageResource="btnContinue" LinkResource="checkout_parentguardian_aspx"  CausesValidation="true" />
<br />

<asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger"  />
    
   
        <asp:HiddenField  id="NoQuals" value="false" runat="server"  ClientIDMode="static" />
     <asp:HiddenField  id="intQualRows" value="0" runat="server"  ClientIDMode="static" />

<script type="text/javascript">

    $(document).ready(function () {
        // Hide & show details on fees (without popping the browser window to the top on each click!)
        $(".slidingDiv").show();
        $(".show_hide").show();
        


        $('.show_hide').click(function (e) {
            $(".slidingDiv").slideToggle();
            var qual = (document.getElementById('NoQuals').value);
            qual = (qual == "true") ? true : false;
            document.getElementById('NoQuals').value = !qual;
            e.preventDefault();
        });
        // End hide & show

    });
</script>
