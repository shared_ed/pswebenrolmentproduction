#region Help:  Introduction to the Script Component
/* The Script Component allows you to perform virtually any operation that can be accomplished in
 * a .Net application within the context of an Integration Services data flow.
 *
 * Expand the other regions which have "Help" prefixes for examples of specific ways to use
 * Integration Services features within this script component. */
#endregion

#region Namespaces
using System;
using Microsoft.SqlServer.Dts.Pipeline.Wrapper;
using System.Net;
using System.IO;
using System.Collections.Generic;
using System.Net.Http;
using Microsoft.SqlServer.Dts.Runtime;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using SmartAssessorShared;
using SmartAssessorBaseClasses;

#endregion
/// <summary>
/// This is the class to which to add your code.  Do not change the name, attributes, or parent
/// of this class.
/// </summary>
[Microsoft.SqlServer.Dts.Pipeline.SSISScriptComponentEntryPointAttribute]
public class ScriptMain : UserComponent
{
    #region Help:  Using Integration Services variables and parameters
    /* To use a variable in this script, first ensure that the variable has been added to
     * either the list contained in the ReadOnlyVariables property or the list contained in
     * the ReadWriteVariables property of this script component, according to whether or not your
     * code needs to write into the variable.  To do so, save this script, close this instance of
     * Visual Studio, and update the ReadOnlyVariables and ReadWriteVariables properties in the
     * Script Transformation Editor window.
     * To use a parameter in this script, follow the same steps. Parameters are always read-only.
     *
     * Example of reading from a variable or parameter:
     *  DateTime startTime = Variables.MyStartTime;
     *
     * Example of writing to a variable:
     *  Variables.myStringVariable = "new value";
     */
    #endregion

    #region Help:  Using Integration Services Connnection Managers
    /* Some types of connection managers can be used in this script component.  See the help topic
     * "Working with Connection Managers Programatically" for details.
     *
     * To use a connection manager in this script, first ensure that the connection manager has
     * been added to either the list of connection managers on the Connection Managers page of the
     * script component editor.  To add the connection manager, save this script, close this instance of
     * Visual Studio, and add the Connection Manager to the list.
     *
     * If the component needs to hold a connection open while processing rows, override the
     * AcquireConnections and ReleaseConnections methods.
     * 
     * Example of using an ADO.Net connection manager to acquire a SqlConnection:
     *  object rawConnection = Connections.SalesDB.AcquireConnection(transaction);
     *  SqlConnection salesDBConn = (SqlConnection)rawConnection;
     *
     * Example of using a File connection manager to acquire a file path:
     *  object rawConnection = Connections.Prices_zip.AcquireConnection(transaction);
     *  string filePath = (string)rawConnection;
     *
     * Example of releasing a connection manager:
     *  Connections.SalesDB.ReleaseConnection(rawConnection);
     */
    #endregion

    #region Help:  Firing Integration Services Events
    /* This script component can fire events.
     *
     * Example of firing an error event:
     *  ComponentMetaData.FireError(10, "Process Values", "Bad value", "", 0, out cancel);
     *
     * Example of firing an information event:
     *  ComponentMetaData.FireInformation(10, "Process Values", "Processing has started", "", 0, fireAgain);
     *
     * Example of firing a warning event:
     *  ComponentMetaData.FireWarning(10, "Process Values", "No rows were received", "", 0);
     */
    #endregion

    public override void Input0_ProcessInputRow(Input0Buffer Row)
    {
        String result;
        String actionTaken;

        try
        {
            Assessor assessor = new Assessor();

            assessor.Id = Row.Id;
            assessor.OrganisationId = Row.OrganisationId;
            assessor.Name = Row.Name;
            assessor.AdminLevel = Convert.ToInt32(Row.AdminLevel);
            assessor.Email = Row.Email;
            assessor.AdminFlags = Convert.ToInt32(Row.AdminFlags);
            assessor.Assessment = Row.Assessment;
            assessor.FirstName = Row.FirstName;
            assessor.LastName = Row.LastName;
            assessor.AdminType = Convert.ToInt32(Row.AdminType);
            assessor.Phone = Row.Phone;
            assessor.Mobile = Row.Mobile;
            assessor.A1Qualification = Convert.ToBoolean(Row.A1Qualification);
            assessor.DefaultIV = Row.DefaultIV;
            assessor.ReadOnly = Convert.ToBoolean(Row.ReadOnly);
            assessor.RiskRating = Convert.ToInt32(Row.RiskRating);
            assessor.NonQualifiedIV = Convert.ToBoolean(Row.NonQualifiedIV);
            assessor.Region = Row.Region;
            assessor.MISAssessorId = Row.MISAssessorId;
            assessor.ViewSessionFeedback = Convert.ToBoolean(Row.ViewSessionFeedback);
            assessor.SelectedAssessors = Row.SelectedAssessors;
            assessor.TimeZone = Row.TimeZone;
            assessor.CreateEmailOnCreation = Convert.ToBoolean(Row.CreateEmailOnCreation);
           
            String strJson = JsonConvert.SerializeObject(assessor);

            if (assessor.Id == "0")
            {
                actionTaken = "Insert";
                result = SmartAssessor.PostRequest("Assessor/CreateAssessor", strJson);
            }
            else
            {
                actionTaken = "Update";
                result = SmartAssessor.PostRequest("Assessor/UpdateAssessor", strJson);
            }

            if (result.Length != 0 && !result.Equals("true")) //assume a failure
            {
                Output0Buffer.AddRow();
                Output0Buffer.Target = "Assessor";
                Output0Buffer.ActionTaken = actionTaken;
                Output0Buffer.MisId = assessor.MISAssessorId;
                Output0Buffer.Name = assessor.Name;
                Output0Buffer.ErrorDescription = result;
                Output0Buffer.Json = strJson;
                Output0Buffer.Created = Convert.ToString(DateTime.Now);
            }
        }
        catch (Exception e)
        {
            FailComponent(e.ToString());
        }

    }

    private void FailComponent(string errorMsg)
    {
        bool fail = false;
        IDTSComponentMetaData100 compMetadata = this.ComponentMetaData;
        compMetadata.FireError(1, "Error Getting Data From Webservice!", errorMsg, "", 0, out fail);
    }

}
