<%@ control Language="VB" AutoEventWireup="false" CodeFile="checkout_prior_attainment.vb" Inherits="checkout_prior_attainment"  %>
<%@ Register Assembly="PSWebEnrolmentKit" Namespace="CompassCC.ProSolution.PSWebEnrolmentKit"
    TagPrefix="cc1" %>

<br /><br />
                <ol class="breadcrumb" id="breadcrumbapps" runat="server">
                   <li><a href="http://www.liv-coll.ac.uk/All_Courses.aspx">Search</a></li>
                    <li><a href="#" onclick="window.history.go(-5);return false;">Course Details</a></li>
                    <li><a href="#" onclick="window.history.go(-4);return false;">Your Courses</a></li>
					<li><a href="#" onclick="window.history.go(-3);return false;">Personal Details</a></li>
					<li><a href="#" onclick="window.history.go(-2);return false;">Further Details</a></li>
					<li><a href="#" onclick="window.history.go(-1);return false;">School/Employer</a></li>
                 <li class="active">Prior Attainment</li>
                </ol>
				
                <ol class="breadcrumb" id="breadcrumbenrols" runat="server">
                  <li><a href="default.aspx">Home</a></li>
                    <li><a href="webenrolment.aspx?page=~/webcontrols/checkout.ascx">Checkout</a></li>
                    <li><a href="webenrolment.aspx?page=~/webcontrols/checkout_enrolments.ascx">Personal Details</a></li>
                   
                    <li><a href="webenrolment.aspx?page=~/webcontrols/checkout_enrolments2.ascx">Further Details</a></li>
                    <li><a href="webenrolment.aspx?page=~/webcontrols/checkout_parent_guardian.ascx">Parent/Carer</a></li>
                    <li><a href="webenrolment.aspx?page=~/webcontrols/checkout_school_employer.ascx">School / Employer</a></li>
                  <li class="active">Prior Attainment</li>
                </ol>

<div class="panel panel-info" id="panel1" runat="server">
    <div class="panel-heading">Qualifications Gained</div>
    <div class=" form-group">
    <cc1:studentenrolmentfield id="StudentEnrolmentField17" runat="server" customcaption="What is your highest level of Qualification?"
         isrequired="True" labelwidth="300" StudentEnrolmentFieldType="PriorAttainmentLevelID"></cc1:studentenrolmentfield>
    </div>
    
    <p>
        Use the following table to help you pick the correct qualification level...</p>
<table class="table table-striped table-bordered">
    <tr class="active"><th>Level</th><th>Qualification Equivalent</th></tr>
 
        <tr>
            <td>
                Level 4</td>
            <td>
                <ul>
                    <li>Foundation Degree</li>
                    <li>Higher National Diploma</li>
                    <li>Higher National Certificate</li>
                </ul>
            </td>
         </tr>
        <tr>
            <td>
                Level 3</td>
            <td>
                <ul>
                    <li>AS/A-level</li>
                    <li>Access to Higher Education</li>
                    <li>BTEC National Award, Certificate, Diploma</li>
                    <li>NVQ Level 3</li>
                    <li>The Advanced Diploma</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                Level 2</td>
            <td>
               <ul>
                    <li>Pre Access</li>
                    <li>BTEC First Diploma</li>
                    <li>NVQ Level 2</li>
                    <li>GCSE grades A to C / 9 to 4</li>
                    <li>The Higher Diploma</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                Level 1</td>
            <td>
               <ul>
                    <li>NVQ Level 1</li>
                    <li>College First</li>
                    <li>GCSE grades D to E / 1 to 3</li>
                    <li>The Foundation Diploma</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                Pre-entry & Entry Level</td>
            <td>
                <ul>
                    <li>Pre-entry and entry level qualifications</li>
                </ul>
           </td>
        </tr>

    </table>

    </div>
		<cc1:CCCButton id="btnBack" runat="server" Text="Back" ImageResource="btnBack" LinkResource="checkout_school_employer_aspx" CausesValidation="false"/>
        <cc1:CCCButton ID="btnContinue" runat="server" Text="Continue" ImageResource="btnContinue" LinkResource="checkout_quals_on_entry_aspx" CausesValidation="true" />
    <br />
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" />


