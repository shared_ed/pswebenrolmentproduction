Option Explicit On
Option Strict On

Imports CompassCC.CCCSystem.CCCCommon
Imports CompassCC.ProSolution.PSWebEnrolmentKit

Partial Class checkout_applications
    Inherits CheckoutBaseControl

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)

        If WorkingData.ShoppingCart.Items.Count = 0 Then
            Response.Redirect(GetResourceValue("COLC_Courses"))
        End If
        'If PaymentSubmitter.AllowEmptyBasket And WorkingData.ShoppingCart.Items.Count = 0 Then
        '    Session("RequestMode") = RequestMode.ApplicationRequest
        'End If

        If Not IsPostBack Then

            postcode.Value = WorkingData.EnrolmentRequestRow.PostcodeOut & WorkingData.EnrolmentRequestRow.PostcodeIn
            WorkingData.EnrolmentRequestRow.RestrictedUseAllowContactByEmail = True
        End If
    End Sub

    Private Sub btnContinue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContinue.Click



        Me.Page.Validate()

        If Me.Page.IsValid Then

            If Len(datepicker.Value) = 10 Then 'assume completed
                Dim dob As DateTime
                Dim age As Integer


                dob = DateTime.Parse(CType(datepicker.Value, String))
                age = Today.Year - dob.Year
                If (dob > Today.AddYears(-age)) Then age -= 1

                WorkingData.StudentDetailRow.Age = CType(age, Short)

            End If

            'postcode stuff            
            If Len(postcode.Value.Trim) > 0 Then
                WorkingData.EnrolmentRequestRow.PostcodeOut = postcode.Value.Trim.Substring(0, postcode.Value.Trim.Length - 3).Trim
                WorkingData.EnrolmentRequestRow.PostcodeIn = Right(postcode.Value.Trim, 3).Trim

                WorkingData.ApplicationRequestRow.PostcodeOut = postcode.Value.Trim.Substring(0, postcode.Value.Trim.Length - 3).Trim
                WorkingData.ApplicationRequestRow.PostcodeIn = Right(postcode.Value.Trim, 3).Trim
            End If


            If FileUpload1.HasFile Then
                Dim fs As System.IO.Stream = FileUpload1.PostedFile.InputStream()
                Dim MyData(CInt(fs.Length)) As Byte
                fs.Read(MyData, 0, CInt(fs.Length))
                fs.Close()

                WorkingData.EnrolmentRequestRow.Photo = MyData
            End If

        End If
    End Sub
End Class
