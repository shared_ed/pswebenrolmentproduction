﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="thankyou_application.ascx.vb" Inherits="webcontrols_thankyou_applications" %>


<h2 class="alert alert-success">
    You have successfully submitted your application.</h2>
    <p>
        Congratulations, you have taken the first step and our team will now progress your application. 
	</p>
	<p>
		Once you become a student of The City of Liverpool College you can access a wide range of discounts and free resources from Office 365 and online courses to discounts at our Award winning Academy Restaurant, our hugely successful MOT centre and stylish Graduate Salon. 
	</p>