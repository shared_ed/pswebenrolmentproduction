﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="checkout_parent_guardian.ascx.vb" Inherits="webcontrols_checkout_parent_guardian" %>

<%@ Register Assembly="PSWebEnrolmentKit" Namespace="CompassCC.ProSolution.PSWebEnrolmentKit"
    TagPrefix="cc1" %>


<br /><br />
                <ol class="breadcrumb" id="breadcrumbapps" runat="server">
                  <li><a href="http://www.liv-coll.ac.uk/All_Courses.aspx">Search</a></li>
                    <li><a href="#" onclick="window.history.go(-7);return false;">Course Details</a></li>
                    <li><a href="#" onclick="window.history.go(-6);return false;">Your Courses</a></li>
					<li><a href="#" onclick="window.history.go(-5);return false;">Personal Details</a></li>
					<li><a href="#" onclick="window.history.go(-4);return false;">Further Details</a></li>
					<li><a href="#" onclick="window.history.go(-3);return false;">School/Employer</a></li>
					<li><a href="#" onclick="window.history.go(-2);return false;">Prior Attainment</a></li>
					<li><a href="#" onclick="window.history.go(-1);return false;">Predicted Grades</a></li>
                  <li class="active">Parent/Carer</li>
                </ol>
				
                   <ol class="breadcrumb" id="breadcrumbenrols" runat="server">
                  <li><a href="default.aspx">Home</a></li>
                    <li><a href="webenrolment.aspx?page=~/webcontrols/checkout.ascx">Checkout</a></li>
                    <li><a href="webenrolment.aspx?page=~/webcontrols/checkout_enrolments.ascx">Personal Details</a></li>
                   
                    <li><a href="webenrolment.aspx?page=~/webcontrols/checkout_enrolments2.ascx">Further Details</a></li>
                  <li class="active">Parent/Carer</li>
                </ol>

<div class="panel panel-success" id="parent1panel" runat="server">
    <div class="panel-heading">Contact Details - Parent/Carer 1</div>
    <div class=" form-group">
        <cc1:StudentEnrolmentField StudentEnrolmentFieldType="ParentFirstName" ID="StudentEnrolmentField2" runat="server" IsRequired="true" LabelWidth="200" />
    </div>
    <div class=" form-group">
        <cc1:StudentEnrolmentField StudentEnrolmentFieldType="ParentSurname" ID="StudentEnrolmentField6" runat="server" IsRequired="true" LabelWidth="200" />
    </div>
    <div class=" form-group">
        <cc1:StudentEnrolmentField StudentEnrolmentFieldType="ParentTitle" ID="StudentEnrolmentField8" runat="server" IsRequired="true" LabelWidth="200" />
    </div>
    <div class=" form-group">
        <cc1:StudentEnrolmentField StudentEnrolmentFieldType="ParentPhoneNumber" ID="StudentEnrolmentField10" runat="server" LabelWidth="200" />
    </div>
    <div class=" form-group">
        <cc1:StudentEnrolmentField StudentEnrolmentFieldType="ParentEmailAddress" ID="StudentEnrolmentField11" runat="server" IsRequired="true" LabelWidth="200" Pattern="^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$"/>
    </div>
    <div class=" form-group">
        <cc1:StudentEnrolmentField StudentEnrolmentFieldType="ParentMobileTel" ID="StudentEmailField1" runat="server" IsRequired="true" LabelWidth="200" />
    </div>
     <div class=" form-group">
        <cc1:StudentEnrolmentField StudentEnrolmentFieldType="IsLivingWithParent" ID="StudentEnrolmentField16" runat="server" IsRequired="false" LabelWidth="200" Width="40" />
    </div>
</div>

<div class="panel panel-success" id="parent2panel" runat="server">
    <div class="panel-heading">Contact Details - Parent/Carer 2</div>
    <div class=" form-group">
        <cc1:StudentEnrolmentField StudentEnrolmentFieldType="Parent2FirstName" ID="StudentEnrolmentField9" runat="server" IsRequired="false" LabelWidth="200" />
    </div>
    <div class=" form-group">
        <cc1:StudentEnrolmentField StudentEnrolmentFieldType="Parent2Surname" ID="StudentEnrolmentField12" runat="server" IsRequired="false" LabelWidth="200" />
    </div>
    <div class=" form-group">
        <cc1:StudentEnrolmentField StudentEnrolmentFieldType="Parent2Title" ID="StudentEnrolmentField13" runat="server" IsRequired="false" LabelWidth="200" />
    </div>
    <div class=" form-group">
        <cc1:StudentEnrolmentField StudentEnrolmentFieldType="Parent2PhoneNumber" ID="StudentEnrolmentField14" runat="server" LabelWidth="200" />
    </div>
    <div class=" form-group">
        <cc1:StudentEnrolmentField StudentEnrolmentFieldType="Parent2EmailAddress" ID="StudentEnrolmentField15" runat="server"  LabelWidth="200" Pattern="^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$"/>
    </div>
    <div class=" form-group">
        <cc1:StudentEnrolmentField StudentEnrolmentFieldType="Parent2MobileTel" ID="StudentEmailField2" runat="server" IsRequired="false" LabelWidth="200" />
    </div>
    <div class=" form-group">
        <cc1:StudentEnrolmentField StudentEnrolmentFieldType="IsLivingWithParent2" ID="StudentEnrolmentField17" runat="server" IsRequired="false" LabelWidth="200"  Width="40"/>
    </div>
</div>


        <cc1:CCCButton id="btnBack" runat="server" Text="Back" ImageResource="btnBack"  LinkResource="checkout_quals_on_entry_aspx"/>
        <cc1:CCCButton ID="btnContinue" runat="server" Text="Continue" ImageResource="btnContinue"  CausesValidation="true" LinkResource="checkout_dataprotection_apponly_aspx"/>
    <br />
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="" />

