﻿<%@ WebHandler Language="VB" Class="ApplicationRequestHandler" %>

Imports System
Imports System.Web
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.Data
Imports System.Linq
Imports CompassCC.ProSolution.PSWebEnrolmentKit
Imports CompassCC.CCCSystem.CCCCommon

Public Class ApplicationRequestHandler : Implements IHttpHandler, IRequiresSessionState

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"

        If HttpContext.Current.Request("EnrolmentRequest") Is Nothing Then

            context.Response.StatusCode = 500
            context.Response.StatusDescription = "Invalid Data"
            context.Response.Write("Error processing data - EnrolmentRequest object not found")

        Else

            Dim json As String = HttpContext.Current.Request("EnrolmentRequest")
            Dim dt As EnrolmentRequestDataTable = Tabulate(json)

            Dim PaymentID As Integer = -1

            Dim tblOffering As New OfferingDataTable
            Dim v As CCCDataViewDataSet = CCCDataViewDataSet.CreateDataView(tblOffering)
            v.Columns.AddPKColumns()
            v.Columns.EnsureColumnsAreSelected(True, False, tblOffering.WebSiteAvailabilityIDColumn, tblOffering.CourseInformationIDColumn, tblOffering.TotalFeeAmountColumn)
            v.Filters.SetColumnFilter(tblOffering.OfferingIDColumn, dt(0).OfferingID)
            tblOffering.TableAdapter.Load(tblOffering, v)

            If tblOffering(0).TotalFeeAmount > 0 Then
                Dim tbl As New WebPaymentDataTable
                PaymentID = tbl.TableAdapter.GetNextPaymentID()
            End If

            dt(0).AcademicYearID = OfferingDataTable.GetAcademicYearForOffering(dt(0).OfferingID)

            If PaymentID = -1 Then
                dt(0).PaymentStatus = "Paid"
            Else
                dt(0).WebPaymentID = PaymentID
                dt(0).PaymentStatus = "Pending"
            End If

            dt.TableAdapter.Save(dt)

            context.Response.Write("Saved")
        End If

    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Shared Function Tabulate(json As String) As EnrolmentRequestDataTable
        Dim jsonLinq = JObject.Parse(json)

        ' Find the first array using Linq
        Dim srcArray = jsonLinq.Descendants().Where(Function(d) TypeOf d Is JArray).First()
        Dim trgArray = New JArray()
        For Each row As JObject In srcArray.Children(Of JObject)()
            Dim cleanRow = New JObject()
            For Each column As JProperty In row.Properties()
                ' Only include JValue types
                If TypeOf column.Value Is JValue Then
                    cleanRow.Add(column.Name, column.Value)
                End If
            Next

            trgArray.Add(cleanRow)
        Next

        Return JsonConvert.DeserializeObject(Of EnrolmentRequestDataTable)(trgArray.ToString())
    End Function

End Class