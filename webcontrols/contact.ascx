﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="contact.ascx.vb" Inherits="webcontrols_contact" %>

<br /><br />
<ol class="breadcrumb">
    <li class="active">Contact Us</li>
</ol>
    <h3>
        Contact Us</h3>

<address>
  <strong>The City of Liverpool College</strong><br>
  Learning Exchange<br>
  Roscoe Street<br>
  Liverpool<br> L1 9DW<br>
  <abbr title="Phone">Tel:</abbr> (0151) 252 3000<br>

  <abbr title="Email">Email:</abbr> <a href="mailto:enquiries@liv-coll.ac.uk">enquiries@liv-coll.ac.uk</a>
</address>


    <p>
        For student Finance queries please email <a href="mailto:askann@liv-coll.ac.uk ">askann@liv-coll.ac.uk </a></p>
    
    <br />
    <h3>
        Location map</h3>
    <p>
        For more information on where the college is located, please see <a href="http://www.liv-coll.ac.uk/College-Locations-i7.html">here</a> </p>


